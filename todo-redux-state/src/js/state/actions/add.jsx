import uuidv1 from 'uuid/v1';

// Reducer action type
export const ADD_TODO = 'ADD_TODO';

/**
 * Create an add action for the reducer to handle.
 * @param {string} title - title of new todo to add.
 * @return {*} action for reducer.
 */
export const addTodo = title => ({
  type: ADD_TODO,
  id: uuidv1(),
  title: title
});
