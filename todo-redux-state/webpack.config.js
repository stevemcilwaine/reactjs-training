/* eslint-disable */
const webpack = require('webpack');
const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin');
const WebpackShellPlugin = require('webpack-shell-plugin');
const path = require('path');
const autoprefixer = require('autoprefixer');
const JsDocPlugin = require('jsdoc-webpack4-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = (env, argv) => {
  const buildMode = argv.mode || 'development';

  return {
    mode: 'development',
    devServer: {
      contentBase: path.join(__dirname, './dist'),
      port: 3003
    },
    devtool: 'source-map',
    entry: ['./src/js/app.jsx', './src/css/styles.scss'],
    output: {
      path: path.resolve(__dirname, 'dist/'),
      filename: 'app.bundle.js'
    },
    module: {
      rules: [{
        test: /\.jsx$/,
        use: [{
          loader: 'babel-loader',
          options: {
            presets: ['react']
          }
        }]
      }, {
        test: /\.js$/,
        exclude: [
          path.resolve(__dirname, 'node_modules')
        ],
        use: [{
          loader: 'babel-loader'
        }]
      }, {
        test: /\.scss$/,
        use: [
          "style-loader", // creates style nodes from JS strings
          "css-loader",  // translates CSS into CommonJS
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [autoprefixer()]
            }
          },
          "sass-loader" // compiles Sass to CSS, using Node Sass by default
        ]
      }, {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "url-loader?limit=10000&mimetype=application/font-woff"
      }, {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "file-loader"
      }, {
        test: /\.jsx$/,
        exclude: /node_modules/,
        loader: "eslint-loader",
        options: {
          configFile: '.eslintrc'
        }
      }, {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: [{
          loader: 'file-loader'
        }, {
          loader: 'kraken-loader',
          options: {
            enabled: buildMode === 'production',
            secret: '59e2f4202e04bf55955d938055fd2bae6fa15f15',
            silent: false,
            lossy: false,
            key: '6f110caf166a4e6760b90eb8e3befc22'
          }
        }]
      }]
    },
    plugins: [
      new CopyWebpackPlugin([{
        from: 'assets/**/*',
        transformPath (targetPath, absolutePath) {
          return targetPath.substr('assets/'.length);
        }
      }], {
        debug: 'info'
      }),
      new SVGSpritemapPlugin({
          // Optional options object
          src: './icons/*.svg'
      }),
      new WebpackShellPlugin({
        onBuildStart: ['npm run lint-style'],
        dev: false
      }),
      new JsDocPlugin({
        conf: './jsdoc.conf'
      })
    ]
  };
};
