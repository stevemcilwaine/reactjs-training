z-course-todo
============

Set Up
======

## Set up - install dependencies

`npm install`

Dev
===

## Run Dev Server - This command will build the project with linting, watch the source files and start a live reload server on http://localhost:8080

`npm run serve`

## Build Only - To only build, lint and watch, no live server, you can use the following command

`npm run watch`

## Stand Alone Lint - This will run a lint over all source files ( JSX ) in the src/js folder ( eslint )

`npm run lint`

Builds
======

## Build - This will build the project and write the files to the dist folder, it's set to do a production build, so output will be uglified.

`npm run bundle`
