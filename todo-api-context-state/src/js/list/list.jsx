import React from 'react';

import {Add} from './add/add.jsx';
import {Item} from './item/item.jsx';
import {TodoConsumer, TodoProvider} from '../todo-provider.jsx';

/**
 * List component.
 */
export class List extends React.Component {
  /**
   * Render.
   * @return {JSX} - template.
   */
  render() {
    return (
      <TodoProvider>
        <TodoConsumer>
          {list => (
            <Add
              add={list.add}
            ></Add>
          )}
        </TodoConsumer>
        <TodoConsumer>
          {list => list.todos.map(todo => (
            <Item
              todo={todo}
              key={todo.id}
              delete={list.delete}
              toggle={list.toggleDone}
            ></Item>
          ))}
        </TodoConsumer>
      </TodoProvider>
    );
  }
}
