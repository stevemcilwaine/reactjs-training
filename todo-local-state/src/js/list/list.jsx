import React from 'react';

import uuidv1 from 'uuid/v1';

import {Add} from './add/add.jsx';
import {Item} from './item/item.jsx';

/**
 * List component.
 */
export class List extends React.Component {
  state = {
    todos: [{
      id: 0,
      done: false,
      title: 'Get shopping'
    }, {
      id: 1,
      done: true,
      title: 'Have coffee'
    }, {
      id: 2,
      done: false,
      title: 'Take out rubbish'
    }]
  };

  /**
   * Add a new todo.
   * @param {string} title - title of todo to add.
   * @return {*} new state with todo added.
   */
  add = title => this.setState(state => ({
    todos: [
      ...state.todos,
      {
        id: uuidv1(),
        done: false,
        title
      }
    ]
  }));

  /**
   * Delete a todo.
   * @param {number} id - id of todo to delete.
   * @return {*} new state with todo removed.
   */
  delete = id => this.setState(state => ({
    todos: state.todos.filter(todo => todo.id !== id)
  }));

  /**
   * Toggle done state for the given todo.
   * @param {number} id - id of todo to toggle.
   * @return {*} new state with todo done state toggled.
   */
  toggleDone = id => this.setState(state => ({
    todos: state.todos.map(todo => todo.id === id ? {...todo, done: !todo.done} : todo)
  }));

  /**
   * Render.
   * @return {JSX} - template.
   */
  render() {
    return (
      <React.Fragment>
        <Add
          add={this.add}
        ></Add>
        {this.state.todos.map(todo => (
          <Item
            todo={todo}
            key={todo.id}
            delete={this.delete}
            toggle={this.toggleDone}
          ></Item>
        ))}
      </React.Fragment>
    );
  }
}
